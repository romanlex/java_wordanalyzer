Home work №1
------

Example arguments for start programm:
```
/home/roman/IdeaProjects/wordanalyzer/files/file1.txt
/home/roman/IdeaProjects/wordanalyzer/files/file2.txt
/home/roman/IdeaProjects/wordanalyzer/files/file3.txt
site.ru/file4.txt
https://gist.githubusercontent.com/romanlex/bbe28fdba5e7f4ed740b1b3671b0a37e/raw/53a76cb57bea28e16e6804b6f6d40a459d05cdd0/file5.txt
file://home/roman/IdeaProjects/wordanalyzer/site.ru/file6.txt
https://gist.githubusercontent.com/romanlex/5e0359bc9fc78860c34ca2d68389a588/raw/3322cf68caf5e7caaa7a95327032db03fbd41281/file6.txt
//localhost/file7.txt
c:\tmp\winfile1.txt
ftp://site.ru/text4.txt
```