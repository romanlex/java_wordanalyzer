package resource;

import java.io.*;
import java.nio.file.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import providers.*;
import validator.Validator;
import org.apache.commons.validator.routines.*;

public class Resource {

    //public static final Object locker = new Object();
    private final ReentrantLock lock = new ReentrantLock();
    public static HashSet words = new HashSet();
    public static volatile boolean stop = false;
    public static final String REGEX_DISALLOW_CHARS = "^[а-яА-ЯёЁ0-9-—:;«»\\?!\\.\\,\\s]{1,}$";
    public static final String REGEX_FIND_WORD = "\\b[а-яА-ЯёЁ0-9-]{2,}+\\b";
    public static final boolean SHOW_BUFFER_IN_CONSOLE = false;
    public static final String acceptCharset = "UTF-8";

    private static final String[] urlSchemes = {"http", "https"};
    private String path;
    private File file;
    private String resourceText = "";

    public static final Logger log = Logger.getLogger(Resource.class);

    static {
        DOMConfigurator.configure("src/main/resources/logger-config.xml");
    }

    /**
     * Resource object
     * @param path Path of file
     */
    public Resource(String path) {
        this.path = path;
    }

    /**
     * Open resource from path string
     * @throws IOException Error on load resource
     */
    public void load() throws IOException {
        if(stop)
            return;

        log.debug("Open resource from path: " + this.path);

        try {
            loadingProvider();
        } catch (URISyntaxException e) {
            log.debug(e.getMessage());
        }

        if(Files.exists(Paths.get(this.path)))
        {
            log.debug("File exist. Open file from hard disk (" + this.path + ")");
            try (InputStreamReader is = new InputStreamReader(new FileInputStream(path), Resource.acceptCharset)) {
                BufferedReader br = new BufferedReader(is);
                String filename = this.getFilename();
                resourceText = this.readResource(br);

                if(SHOW_BUFFER_IN_CONSOLE)
                    System.out.println("-> Getting text from buffer:\n_____________________" + filename + "_____________________\n" +
                            resourceText + "\n______________ end of file " + filename + "_______________");
            } catch (UnsupportedEncodingException e) {
                log.warn("Cannot read file with accept charset " + Resource.acceptCharset + ". UTF-8 not supported.");
            } catch (IOException e) {
                log.error(e.getMessage());
                log.error("Stop threads");
            } finally {

                if(this.file != null && this.file.delete())
                    log.debug("Temp file is removed from hard disk.");

            }
        } else {
            log.warn("File not exist. Cannot open file from this path: " + this.path);
        }

    }

    private void loadingProvider() throws URISyntaxException {
        UrlValidator urlValidator = new UrlValidator(urlSchemes);
        String scheme = this.detectHandler(this.path);

        if (urlValidator.isValid(this.path)) {
            log.debug("Path is valid URL (" + this.path + ")");
            scheme = "http";
        } else {
            log.debug("Trying identify the provider handler by protocol in path.");
        }

        try {
            startProvider(scheme);
        } catch (ClassNotFoundException e) {
            log.warn("Handler class for this protocol " + scheme + " not found");
        } catch (IllegalAccessException e) {
            log.fatal("Get IllegalAccessException");
            log.trace(e.getStackTrace());
        } catch (InstantiationException e) {
            log.fatal("Get InstantiationException");
            log.trace(e.getStackTrace());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }


    private void startProvider(String protocol) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        log.debug("Protocol is: " + protocol);
        log.debug("Load custom class");
        String providerName = capitalize(protocol) + "Provider";
        String packageFullName = "providers." + providerName;
        log.debug("Starting " + providerName + " Handler from " + packageFullName);

        Providers provider = (Providers) Class.forName(packageFullName).newInstance();
        provider.setUrl(this.path);

        if(provider.getFile()) {
            this.file = provider.getDownloadedFile();
            this.path = this.file.getAbsolutePath();
        } else {
            log.error("Cannot get file from this provider: " + providerName);
        }
    }


    /**
     * Read resource from BufferedReader and validate his. Return string if SHOW_BUFFER_IN_CONSOLE is true.
     * @param bufferedReader BufferedReader instance for read file
     * @return String
     * @throws IOException Error when reading resource
     */
    private String readResource(BufferedReader bufferedReader) throws IOException {
        String inputLine;
        String text = "";

        while (!stop && (inputLine = bufferedReader.readLine()) != null) {
            if(SHOW_BUFFER_IN_CONSOLE)
                text += inputLine;

            if(!Validator.checkWithRegExp(inputLine, REGEX_DISALLOW_CHARS))
                throw new IOException("[STOP] Ignoring file. File " + this.getFilename() + " does not satisfy the conditions.\nAccept only cyrillic symbols, punctuation symbols and alphanumeric.");

            log.debug("File is valid. Parsing...");

            ArrayList<String> matches = this.getWordsByRegExp(inputLine, REGEX_FIND_WORD);
            log.info("Found " + matches.size() + " words in " + this.path);

            lock.lock();
            try {
                for (String word:
                        matches) {
                    if(!words.add(word)) {
                        stop = true;
                        throw new IOException("\n------------------------------------------------------\n" +
                                "############   Word '" + word + "' is already found in the list. File \""  + this.path + "\" ignoring. Exit." +
                                "\n------------------------------------------------------");
                    }

                }
            } finally {
                lock.unlock();
            }

        }
        bufferedReader.close();
        return text;
    }

    /**
     * Get words from string by REGEX
     * @param str String
     * @param regex Regex
     * @return ArrayList<String>
     */
    public ArrayList<String> getWordsByRegExp(String str, String regex) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        ArrayList<String> words = new ArrayList<>();
        while (m.find()) {
            words.add(m.group());
        }
        return words;
    }


    /**
     * Method detect protocol of path (file:// ftp://)
     * @param path String of file path
     * @return String protocol
     * @throws URISyntaxException URI does not contain protocol
     */
    public String detectHandler(String path) throws URISyntaxException {
        log.debug("Detecting handler by protocol (path: " + path + ")");
        String scheme = this.getScheme(path);

        if (scheme == null || scheme.isEmpty() ) {
            String msg = "Protocol not found in path";
            throw new URISyntaxException(msg, "Ignoring");
        }

        return scheme;
    }

    /**
     * Get scheme from string (example URL)
     * @param path String of path
     * @return Protocol from path
     */
    private String getScheme(String path) {
        try {
            URI uri = new URI(path);
            return uri.getScheme();
        }
        catch (Exception e) {
            return "";
        }
    }

    /**
     * Capitalize first letter in string
     * @param str String where capitalize first letter
     * @return Capitalized string
     */
    private String capitalize(final String str) {
        return Character.toUpperCase(str.charAt(0)) + str.substring(1);
    }


    /**
     * Return filename of current resource
     * @return Filename
     */
    public String getFilename() {
        return Paths.get(this.path).getFileName().toString();
    }

    /**
     * Return File if file loaded from link or another protocol
     * @return File
     */
    public File getFile() {
        return file;
    }
}
