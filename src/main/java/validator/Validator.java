package validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    /**
     * Check string by regex
     * @param str
     * @param regex
     * @return boolean
     */
    public static boolean checkWithRegExp(String str, String regex) {
        if(str == null)
            return false;
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        return m.matches();
    }
}
