import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import resource.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    private static final boolean MULTI_THREAD_SUPPORT = true;
    private static ArrayList<Thread> threads = new ArrayList<>();
    public static final Logger log = Logger.getLogger(Main.class);

    static {
        DOMConfigurator.configure("src/main/resources/logger-config.xml");
    }

    public static void main(String[] args) {
        if(args.length == 0) {
            log.error("Program wait arguments for work. Please run program with arguments: filepath or url to textfile");
            return;
        }
        initializeArguments(args);
    }

    /**
     * Start programm with arguments
     * @param args
     */
    public static void initializeArguments(String[] args) {
        log.debug("Found " + args.length + " arguments from command line.");
        log.debug("Start resource parsing process.");

        for (String arg : args) {
            processArgument(arg);
        }

        for (Thread thread:
             threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("\n\nFound " + Resource.words.size() + " unique words in files:");
        System.out.println(Arrays.asList(Resource.words));
    }

    public static void processArgument(String arg) {
        if(MULTI_THREAD_SUPPORT) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    runProccess(arg);
                }
            });
            thread.start();
            threads.add(thread);
        } else {
            runProccess(arg);
        }
    }

    private static void runProccess(String arg) {
        try {
            loadResourceByArg(arg);
        } catch (FileNotFoundException e) {
            log.warn(e.getMessage());
        } catch (InterruptedException e) {
            log.trace(e.getStackTrace());
        } catch (IOException e) {
            log.warn(e.getMessage());
        }
    }

    public static void loadResourceByArg(String arg) throws IOException, InterruptedException {
        Resource resource = new Resource(arg);
        resource.load();
    }
}