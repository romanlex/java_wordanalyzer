package providers;
/*
* Handler for file:// protocol
*/

import java.io.File;

public class FileProvider extends Providers {
    public FileProvider() {

    }

    @Override
    public boolean getFile() {
        return false;
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public int getStatus() {
        return 0;
    }

    @Override
    public File getDownloadedFile() {
        return null;
    }
}
