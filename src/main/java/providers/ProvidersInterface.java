package providers;

import java.io.File;
import java.net.MalformedURLException;

/**
 * Created by roman on 10.02.17.
 */
public interface ProvidersInterface {
    void load();
    boolean getFile();
    int getSize();
    int getStatus();
    File getDownloadedFile();
    void setUrl(String path) throws MalformedURLException;
}
