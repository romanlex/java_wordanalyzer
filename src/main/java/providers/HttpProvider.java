package providers;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import resource.Resource;

import java.io.*;
import java.net.HttpURLConnection;

/*
* Handler for http & https protocol
*/

public class HttpProvider extends Providers {

    public static final Logger log = Logger.getLogger(Resource.class);

    static {
        DOMConfigurator.configure("src/main/resources/logger-config.xml");
    }

    public HttpProvider() {

    }

    /**
     * Open HTTP Connection
     * @return HttpURLConnection
     * @throws IOException
     */
    private HttpURLConnection openHttpConnection() throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        return connection;
    }

    /**
     * Download from URL
     * @param connection
     * @return boolean
     * @throws IOException
     */
    private boolean download(HttpURLConnection connection) {
        try {
            BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
            downloadedFile = new File(this.getRandomFileName());
            FileOutputStream fw = new FileOutputStream(downloadedFile);

            byte[] btBuffer = new byte[MAX_BUFFER_SIZE];
            int intRead = 0;
            while( (intRead = bis.read(btBuffer)) != -1 ){
                fw.write(btBuffer, 0, intRead);
                readBytes = readBytes + intRead;
            }
            fw.close();
            log.debug("Download successfull. File is save to dir: " + downloadedFile.getAbsolutePath().toString());
            return true;
        } catch (IOException ex) {
            log.fatal(ex.getMessage());
        }
        return false;
    }

    @Override
    public boolean getFile() {
        log.debug("-> Starting UrlProvider Handler");
        log.debug("-> Downloading file ...");
        String path = null;
        try {
            HttpURLConnection connection = this.openHttpConnection();
            connection.connect();
            this.status = connection.getResponseCode();
            this.size = connection.getContentLength();

            if (this.status / 100 != 2)
            {
                log.info("-> Ignoring url. Response code: " + Integer.toString(this.status));
                return false;
            }

            if((connection.getContentType() != "text/plain") &&
                    !this.url.toString().endsWith(".txt") ) {
                log.info("-> Content type of this url not text/plain. Change url to text file.");
                return false;
            }

            return this.download(connection);

        }  catch (IOException e) {
            log.debug("Cannot open HTTP connection. ");
        }
        return false;
    }

    @Override
    public int getSize() {
        return this.size;
    }

    @Override
    public int getStatus() {
        return this.status;
    }

    @Override
    public File getDownloadedFile() {
        return downloadedFile;
    }


}
