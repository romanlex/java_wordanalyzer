package providers;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

public class Providers implements ProvidersInterface {

    protected static final int MAX_BUFFER_SIZE = 1024;
    protected File downloadedFile;
    protected URL url;
    protected int size = 0;
    protected int status = 0;
    protected int readBytes = 0;

    /**
     * Loading provider settings and process connect if required
     * @return void
     */
    public void load() {
        System.out.println("-> Downloading file ...");
    }

    @Override
    public boolean getFile() {
        return false;
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public int getStatus() {
        return 0;
    }

    @Override
    public File getDownloadedFile() {
        return null;
    }

    @Override
    public void setUrl(String path) throws MalformedURLException {
        this.url = new URL(path);
    }


    /**
     * Return random name fo file
     * @return String
     */
    public String getRandomFileName() {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

}
