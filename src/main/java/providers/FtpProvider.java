package providers;

/*
* Handler for ftp:// protocol
*/

import java.io.*;

public class FtpProvider extends Providers {
    public FtpProvider() {

    }

    @Override
    public boolean getFile() {
        return false;
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public int getStatus() {
        return 0;
    }

    @Override
    public File getDownloadedFile() {
        return null;
    }
}
