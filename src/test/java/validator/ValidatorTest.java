package validator;

import resource.Resource;
import org.junit.*;

public class ValidatorTest {

    @Test
    public void checkWithRegExp() throws Exception {
        String str = "В октябре 1805 года русские войска занимали села и города эрцгерцогства Австрийского, и еще новые полки приходили из России, и, отягощая постоем жителей, располагались у крепости Браунау.";
        String regex = Resource.REGEX_DISALLOW_CHARS;
        Validator.checkWithRegExp(str, regex);
        System.out.println(Validator.checkWithRegExp(str, regex));
    }
}