package providers;

import org.junit.Test;

/**
 * Created by roman on 10.02.17.
 */
public class HttpProviderTest {

    private String url = "https://gist.githubusercontent.com/romanlex/bbe28fdba5e7f4ed740b1b3671b0a37e/raw/e00cc87b653f24619d48126119a295c900186ac5/file5.txt";

    @Test
    public void load() throws Exception {
        System.out.println("Check load method");
        HttpProvider provider = new HttpProvider();
        provider.load();
        System.out.println("Size:" + provider.getSize());
        System.out.println("Status:" + provider.getStatus());
    }

    @Test
    public void openHttpConnection() throws Exception {

    }

    @Test
    public void getFile() throws Exception {

    }

    @Test
    public void getRandomFileName() throws Exception {

    }

    @Test
    public void getDownloadedFile() throws Exception {

    }

    @Test
    public void setUrl() throws Exception {

    }

}